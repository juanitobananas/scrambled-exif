package com.jarsilio.android.scrambledeggsif

import android.Manifest
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.FileUtils
import androidx.core.content.FileProvider
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.GrantPermissionRule
import com.jarsilio.android.common.extensions.applicationId
import com.jarsilio.android.scrambledeggsif.extensions.parcelable
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.File
import java.io.FileOutputStream

@ExperimentalUnsignedTypes
@RunWith(AndroidJUnit4::class)
class HandleImageInstrumentedTest {
    private val applicationContext = ApplicationProvider.getApplicationContext<App>()
    private val testContext: Context = InstrumentationRegistry.getInstrumentation().context

    private val unscrambledFileStream =
        testContext.resources.assets.open(
            "marvincito.jpg",
        ) // It has to be the TEST Context. If not, we can't access the assets
    private val expectedScrambledFileStream =
        testContext.resources.assets.open(
            "marvincito-scrambled.jpg",
        ) // It has to be the TEST Context. If not, we can't access the assets

    @get:Rule
    val readExternalStorageRuntimePermissionRule: GrantPermissionRule = GrantPermissionRule.grant(Manifest.permission.READ_EXTERNAL_STORAGE)

    private val shareJpegWithHandleImageActivityLaunchIntent: Intent
        get() {
            File(applicationContext.cacheDir, "images").mkdirs()
            // Create a copy of "marvincito.jpg" in fileprovider like defined in AndroidManifest (cache/images), so that we can share it with HandleImageActivity
            val testFileInFileProvider = File("${applicationContext.cacheDir}/images/marvincito.jpg")
            val testFileOutputStreamInFileProvider = FileOutputStream(testFileInFileProvider)
            FileUtils.copy(unscrambledFileStream, testFileOutputStreamInFileProvider)

            val uri =
                FileProvider.getUriForFile(
                    applicationContext,
                    "${applicationContext.applicationId}.fileprovider",
                    testFileInFileProvider,
                )
            return Intent(
                ApplicationProvider.getApplicationContext(),
                HandleImageActivity::class.java,
            ).apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_STREAM, uri)
                type = "image/*"
                addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION) // temp permission for receiving app to read this file
            }
        }

    @Test
    fun testJpegScramblingWithHandleImageActivity() {
        Intents.init()
        ActivityScenario.launch<HandleImageActivity>(shareJpegWithHandleImageActivityLaunchIntent)
        intended(hasAction(Intent.ACTION_CHOOSER))

        for (i in Intents.getIntents()) {
            if (i.action == Intent.ACTION_CHOOSER) {
                val shareIntent: Intent = i.parcelable(Intent.EXTRA_INTENT)!!
                assertEquals(shareIntent.action, Intent.ACTION_SEND)

                val scrambledUri: Uri = shareIntent.parcelable(Intent.EXTRA_STREAM)!!
                val scrambledFileStream = applicationContext.contentResolver.openInputStream(scrambledUri)!!
                // TODO: compare byte-wise
                assertTrue(scrambledFileStream.readBytes().contentEquals(expectedScrambledFileStream.readBytes()))
            }
        }
    }
}
