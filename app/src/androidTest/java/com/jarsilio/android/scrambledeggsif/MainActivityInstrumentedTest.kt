/*
 * Copyright (c) 2022 Juan García Basilio
 *
 * This file is part of Scrambled Exif.
 *
 * Scrambled Exif is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scrambled Exif is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scrambled Exif.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jarsilio.android.scrambledeggsif

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import org.hamcrest.CoreMatchers.containsString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityInstrumentedTest {
    @get:Rule
    var activityScenarioRule = activityScenarioRule<MainActivity>()

    @Test
    fun openImpressum() {
        // Click on a non-visible menu item: https://stackoverflow.com/questions/33965723/espresso-click-menu-item
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)

        val impressumMenuItem =
            getInstrumentation().targetContext.getString(
                com.jarsilio.android.common.R.string.impressum_activity_title,
            )
        onView(withText(impressumMenuItem)).perform(click())
        onView(withText(containsString(IMPRESSUM_TEXT))).check(matches(isDisplayed()))
    }

    @Test
    fun openPrivacyPolicy() {
        // Click on a non-visible menu item: https://stackoverflow.com/questions/33965723/espresso-click-menu-item
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)

        val privacyPolicyMenuItemName =
            getInstrumentation().targetContext.getString(
                com.jarsilio.android.common.R.string.title_activity_privacy_policy,
            )
        onView(withText(privacyPolicyMenuItemName)).perform(click())
        onView(withText(containsString(PRIVACY_POLICY_TEXT))).check(matches(isDisplayed()))
    }

    @Test
    fun openLicenses() {
        // Click on a non-visible menu item: https://stackoverflow.com/questions/33965723/espresso-click-menu-item
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)

        val privacyPolicyMenuItemName =
            getInstrumentation().targetContext.getString(
                R.string.licenses_menu_item,
            )
        onView(withText(privacyPolicyMenuItemName)).perform(click())
        onView(withText(containsString(LICENSE_TEXT))).check(matches(isDisplayed()))
    }

    @Test
    fun openSettings() {
        // Click on a non-visible menu item: https://stackoverflow.com/questions/33965723/espresso-click-menu-item
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)

        val privacyPolicyMenuItemName =
            getInstrumentation().targetContext.getString(
                R.string.settings_menu_item,
            )
        onView(withText(privacyPolicyMenuItemName)).perform(click())
        onView(withText(containsString(privacyPolicyMenuItemName))).check(matches(isDisplayed()))
    }

    companion object {
        const val IMPRESSUM_TEXT = "Angaben gemäß § 5 Telemediengesetz (TMG):"
        const val PRIVACY_POLICY_TEXT = "This privacy policy governs your use of the software application"
        const val LICENSE_TEXT = "Scrambled Exif License"
    }
}
