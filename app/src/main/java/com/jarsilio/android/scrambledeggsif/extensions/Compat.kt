package com.jarsilio.android.scrambledeggsif.extensions

import android.content.Intent
import android.os.Build
import android.os.Parcelable

// From: https://stackoverflow.com/questions/73019160/android-getparcelableextra-deprecated - thanks for that!
inline fun <reified T : Parcelable> Intent.parcelable(key: String): T? =
    when {
        Build.VERSION.SDK_INT >= 33 -> getParcelableExtra(key, T::class.java)
        else ->
            @Suppress("deprecation")
            getParcelableExtra(key)
                as? T
    }

inline fun <reified T : Parcelable> Intent.parcelableArrayList(key: String): ArrayList<T>? =
    when {
        Build.VERSION.SDK_INT >= 33 -> getParcelableArrayListExtra(key, T::class.java)
        else ->
            @Suppress("deprecation")
            getParcelableArrayListExtra(key)
    }
