New in 1.7.10
★ Fix immediate crash no Android 12 while sharing to Scrambled Exif

New in 1.7.9
★ Update Hebrew translation.

New in 1.7.8
★ Update Dutch and French translations.
★ Upgrade a bunch of dependencies.

New in 1.7.7
★ Update traditional Chinese, Czech, Finnish and French translations.
